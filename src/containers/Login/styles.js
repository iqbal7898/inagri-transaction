import styled from 'styled-components';
import TextField from '@material-ui/core/TextField';

export const GlobalContainer = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  width: 100%;
  height: 100vh;
  box-shadow: 0px 3px 3px rgba(0, 0, 0, 0.16);
  overflow: auto
`;

export const LoginContainer = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: space-evenly;
  align-items: center;
  width: 480px;
  height: 500px;
  margin: 15px;
  padding: 15px;
  box-shadow: 3px 3px 3px 3px rgba(0, 0, 0, 0.16);
  border-radius: 5px;
`;

export const FormContainer = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  height: 200px;
`;

export const TextFieldForm = styled(TextField)`
  && .MuiInputBase-root{
    font-family: 'Baloo 2';
  }

  && .MuiFormLabel-root{
    font-family: 'Baloo 2';
  }
`;

export const LoginText = styled.div`
  color: rgb(33, 33, 33);
  font-family: 'Baloo 2';
  font-size: 24px;
  font-weight: bold;
  text-align: left;
`;

export const LoginButton = styled.div`
  height: 42px;
  width: 100%;
  color: #ffffff;
  display: flex;
  flex-direction: row;
  justify-content: center;
  align-items: center;
`;

export const FacebookLoginButton = styled.div`
  height: 42px;
  width: 100%;
  color: #ffffff;
  display: flex;
  flex-direction: row;
  justify-content: center;
  align-items: center;
`;

export const RegisterText = styled.div`
  color: rgb(33, 33, 33);
  font-family: 'Baloo 2';
  font-size: 12px;
  font-weight: bold;
  text-align: left;
`;
/*
 * Home Actions
 *
 * Actions change things in your application
 * Since this boilerplate uses a uni-directional data flow, specifically redux,
 * we have these actions which are the only way your application interacts with
 * your application state. This guarantees that your state is up to date and nobody
 * messes it up weirdly somewhere.
 *
 * To add a new Action:
 * 1) Import your constant
 * 2) Add a function like this:
 *    export function yourAction(var) {
 *        return { type: YOUR_ACTION_CONSTANT, var: var }
 *    }
 */

import {
  CHANGE_USERNAME,
  CHANGE_PASSWORD,
  LOAD_DATA,
  LOAD_DATA_SUCCESS,
  LOAD_DATA_ERROR,
  LOGIN,
  LOGIN_SUCCESS,
  LOGIN_ERROR,
} from './constants';


export function changeUsername(username) {
  return {
    type: CHANGE_USERNAME,
    username,
  };
}

export function changePassword(password) {
  return {
    type: CHANGE_PASSWORD,
    password,
  };
}

export function loadData() {
  return {
    type: LOAD_DATA,
  };
}

export function dataLoaded(tableData) {
  return {
    type: LOAD_DATA_SUCCESS,
    tableData,
  };
}

export function dataLoadingError(error) {
  return {
    type: LOAD_DATA_ERROR,
    error,
  };
}

export function login(username, password) {
  console.log('post');
  return {
    type: LOGIN,
    username,
    password,
  };
}

export function loggedIn(token) {
  return {
    type: LOGIN_SUCCESS,
    token
  };
}

export function loggedInError(error) {
  return {
    type: LOGIN_ERROR,
    error
  };
}

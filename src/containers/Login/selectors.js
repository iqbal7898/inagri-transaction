import { createSelector } from 'reselect';
import { initialState } from './reducer';

/**
 * Direct selector to the homepage state domain
 */

export const selectLoginData = state => state.login || initialState;

export const makeSelectLoading = () =>
  createSelector(
    selectLoginData,
    dataState => dataState.loading,
  );

export const makeSelectError = () =>
  createSelector(
    selectLoginData,
    dataState => dataState.error,
  );

//FORM


export const makeSelectForm = () =>
  createSelector(
    selectLoginData,
    dataState => dataState.form,
  );

export const makeSelectUsername = () =>
  createSelector(
    selectLoginData,
    dataState => dataState.form.username,
  );

export const makeSelectPassword = () =>
  createSelector(
    selectLoginData,
    dataState => dataState.form.password,
  );

//DATA

export const makeSelectTableData = () =>
  createSelector(
    selectLoginData,
    dataState => dataState.data.tableData,
  );

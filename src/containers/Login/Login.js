/*
 * Login
 *
 * This is the first thing users see of our App, at the '/' route
 */

import React, { Fragment, useState, useEffect, memo } from 'react';
import { Helmet } from 'react-helmet';
import { connect } from 'react-redux';
import { compose } from 'redux';
import Img from '../../components/Img/Img';
import Button from '../../components/Button/Button';
import PropTypes from 'prop-types';
import * as Styles from './styles';
import { useInjectSaga } from 'utils/injectSaga';
import { useInjectReducer } from 'utils/injectReducer';
import { createStructuredSelector } from "reselect";
import reducer from './reducer';
import saga from './saga';
import {
  makeSelectError,
  makeSelectUsername,
  makeSelectPassword
} from "./selectors";
import {
  loadData,
  changeUsername,
  changePassword,
  login,
} from "./actions";

export function Login({
  username,
  password,
  onLoadData,
  onChangeUsername,
  onChangePassword,
  onLogin,
  history
}) {
  useInjectReducer({ key: 'login', reducer });
  useInjectSaga({ key: 'login', saga });

  useEffect(() => {
    onLoadData();
  }, [onLoadData, history]);

  return (
    <Fragment>
      <Helmet>
        <meta charset="utf-8" />
        <title>Inagri - Supplier Sayur Buah Sembako Online</title>
        <meta
          name="description"
          content="Inagri - Supplier Sayur Buah Sembako Online"
        />
      </Helmet>
      <Styles.GlobalContainer>
        <Styles.LoginContainer>
          <a href="http://www.inagri.asia/">
            <Img
              className="insanLogo"
              src={require('../../assets/images/inagri logo.png')}
              height="37px"
              width="160px"
              alt="insan logo"
            />
          </a>
          <Styles.FormContainer>
            <Styles.TextFieldForm
              variant="standard"
              label="Username"
              placeholder="Username"
              style={{ fontFamily: 'Baloo 2' }}
              fullWidth
              value={username}
              onChange={onChangeUsername}
            />

            <Styles.TextFieldForm
              variant="standard"
              type="password"
              label="Password"
              placeholder="Password"
              style={{ fontFamily: 'Baloo 2' }}
              fullWidth
              value={password}
              onChange={onChangePassword}
            />

            <Button
              type='submit'
              size='lg'
              onClick={() => {
                console.log('push');
                onLogin(history)
              }}
            >
              <Styles.LoginButton>
                Login
              </Styles.LoginButton>
            </Button>

          </Styles.FormContainer>

        </Styles.LoginContainer>
      </Styles.GlobalContainer>
    </Fragment>
  );
}

Login.propTypes = {
  history: PropTypes.object,
  dispatch: PropTypes.func,
};

const mapStateToProps = createStructuredSelector({
  error: makeSelectError(),
  username: makeSelectUsername(),
  password: makeSelectPassword(),
});

function mapDispatchToProps(dispatch) {
  return {
    onLoadData: id => dispatch(loadData()),
    onChangeUsername: event => dispatch(changeUsername(event.target.value)),
    onChangePassword: event => dispatch(changePassword(event.target.value)),
    onLogin: (history) => dispatch(login(history)),
  };
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

export default compose(
  withConnect,
  memo,
)(Login);

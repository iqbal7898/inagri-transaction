import { call, select, put, takeLatest } from 'redux-saga/effects';
import { request, getRequestURL } from 'utils/request';
import Swal from 'sweetalert2';
import { ACCOUNT_MODEL } from 'utils/constants';
import { LOGIN, FACEBOOK_LOGIN, GOOGLE_LOGIN } from './constants';
import {
  loggedIn,
  loggedInError,
} from './actions';
import {
  makeSelectForm
} from './selectors';

function* login(history) {

  const form = yield select(makeSelectForm());
  try {
    // Call our request helper (see 'utils/request')
    console.log('form', form)
    const response = yield call(
      request,
      `${getRequestURL(ACCOUNT_MODEL)}/login`,
      {
        headers: {
          'Access-Control-Allow-Origin': '*',
        },
        method: "POST",
        data: {
          username: form.username,
          password: form.password
        }
      }
    );

    const { data } = response.data;
    yield put(loggedIn(data));
    history.push(`/`);
  } catch (err) {
    const errorMessage = err.response
      ? err.response.data.error
      : "Username/Password Salah";
    
    let message = null;
    console.log('errorMessage', errorMessage);
    switch (errorMessage) {
      case 'Account with username string not found. ':
        message = 'Username Tidak Ada';
        break;
      case 'Password not match. ':
        message = 'Password Salah';
        break;
      default:
        return false;
    }
    console.log('err.response', err.response);
    Swal.fire('Submit data gagal!', `${message}`, 'error');
    yield put(loggedInError(errorMessage));
  }
}

function* facebookLogin(history, data) {

  try {
    // Call our request helper (see 'utils/request')
    console.log('respData', data.tokenDetail.accessToken)
    const response = yield call(
      request,
      `${getRequestURL(ACCOUNT_MODEL)}/facebook_login`,
      {
        method: "POST",
        data: {
          access_token: data.tokenDetail.accessToken,
        }
      }
    );

    console.log('response', response);
    const respData = response.data.data;
    console.log('respData', respData);

    const responseAccount = yield call(
      request,
      `${getRequestURL(ACCOUNT_MODEL)}/${respData.userid}`,
      {
        method: "GET",
        headers: {
          "Content-Type": "application/json",
          Authorization: respData.token
        }
      }
    );

    console.log('responseAccount', responseAccount);
    yield put(loggedIn(responseAccount.data.data, respData.token));
    history.push(`/`);
  } catch (err) {
    const errorMessage = err.response
      ? err.response.data.error
      : "Username/Password Salah";
    
    console.log('errorMessage', errorMessage);
    Swal.fire('Login gagal!', `${errorMessage}`, 'error');
    history.push(`/register`);
    yield put(loggedInError(errorMessage));
  }
}


function* googleLogin(history, data) {

  try {
    // Call our request helper (see 'utils/request')
    console.log('respData', data.tokenId)
    const response = yield call(
      request,
      `${getRequestURL(ACCOUNT_MODEL)}/google_login`,
      {
        method: "POST",
        data: {
          id_token: data.tokenId,
        }
      }
    );

    console.log('response', response);
    const respData = response.data.data;
    console.log('respData', respData);

    const responseAccount = yield call(
      request,
      `${getRequestURL(ACCOUNT_MODEL)}/${respData.userid}`,
      {
        method: "GET",
        headers: {
          "Content-Type": "application/json",
          Authorization: respData.token
        }
      }
    );

    console.log('responseAccount', responseAccount);
    yield put(loggedIn(responseAccount.data.data, respData.token));
    history.push(`/`);
  } catch (err) {
    const errorMessage = err.response
      ? err.response.data.error
      : "Username/Password Salah";
    
    console.log('errorMessage', errorMessage);
    Swal.fire('Login gagal!', `${errorMessage}`, 'error');
    history.push(`/register`);
    yield put(loggedInError(errorMessage));
  }
}

export default function* loginSaga() {
  yield takeLatest(LOGIN, function* (action) {
    const { username, password } = action;
    yield call(login, username, password);
  });

  yield takeLatest(FACEBOOK_LOGIN, function* (action) {
    const { history, data } = action;
    yield call(facebookLogin, history, data);
  });

  yield takeLatest(GOOGLE_LOGIN, function* (action) {
    const { history, data } = action;
    yield call(googleLogin, history, data);
  });
}

/**
 * Asynchronously loads the component for Profile
 */

import React from 'react';
import loadable from '../../utils/loadable';
import LoadingIndicator from '../../components/LoadingIndicator/LoadingIndicator';

export default loadable(() => import('./Login'), {
  fallback: <LoadingIndicator />,
});

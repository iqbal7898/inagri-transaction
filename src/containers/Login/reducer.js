/*
 * HomeReducer
 *
 * The reducer takes care of our data. Using actions, we can
 * update our application state. To add a new action,
 * add it to the switch statement in the reducer function
 *
 */

import produce from 'immer';
import {
  CHANGE_USERNAME,
  CHANGE_PASSWORD,
  LOAD_DATA,
  LOAD_DATA_SUCCESS,
  LOAD_DATA_ERROR,
  LOGIN,
  LOGIN_SUCCESS,
  LOGIN_ERROR,
} from './constants';

// The initial state of the App
export const initialState = {
  userId: "",
  form: {
    username: "",
    password: "",
  },
  data: {
    tableData: false,
  },
  error: {
    tableData: false,
    fetchData: false
  },
};

/* eslint-disable default-case, no-param-reassign */
const LoginReducer = (state = initialState, action) =>
  produce(state, draft => {
    switch (action.type) {

      case CHANGE_USERNAME:
        draft.form.username = action.username;
        break;

      case CHANGE_PASSWORD:
        draft.form.password = action.password;
        break;
      
      case LOAD_DATA:
        draft.loading = true;
        draft.error = initialState.error;
        draft.data.tableData = false;
        break;

      case LOAD_DATA_SUCCESS:
        draft.data.tableData = [...action.tableData];
        draft.loading = false;
        break;

      case LOAD_DATA_ERROR:
        draft.error.tableData = action.error;
        draft.loading = false;
        break;

      // POST DATA
      case LOGIN:
        console.log('login');
        draft.error = initialState.error;
        draft.loading = false;
        break;

      case LOGIN_SUCCESS:
        console.log("action", action.account);
        draft.userId = action.token._id;
        localStorage.setItem("token", action.token.access_token);
        draft.loading = false;
        break;

      case LOGIN_ERROR:
        draft.loading = false;
        draft.error.fetchData = action.error;
        break;

    }
  });

export default LoginReducer;

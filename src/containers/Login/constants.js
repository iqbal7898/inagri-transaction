/*
 * HomeConstants
 * Each action has a corresponding type, which the reducer knows and picks up on.
 * To avoid weird typos between the reducer and the actions, we save them as
 * constants here. We prefix them with 'yourproject/YourComponent' so we avoid
 * reducers accidentally picking up actions they shouldn't.
 *
 * Follow this format:
 * export const YOUR_ACTION_CONSTANT = 'yourproject/YourContainer/YOUR_ACTION_CONSTANT';
 */



export const CHANGE_USERNAME = 'insan-group-pwa/Login/CHANGE_USERNAME';
export const CHANGE_PASSWORD = 'insan-group-pwa/Login/CHANGE_PASSWORD';

export const LOAD_DATA = 'insan-group-pwa/Login/LOAD_DATA';
export const LOAD_DATA_SUCCESS = 'insan-group-pwa/Login/LOAD_DATA_SUCCESS';
export const LOAD_DATA_ERROR = 'insan-group-pwa/Login/LOAD_DATA_ERROR';

export const LOGIN = 'insan-group-pwa/Login/LOGIN';
export const LOGIN_SUCCESS = 'insan-group-pwa/Login/LOGIN_SUCCESS';
export const LOGIN_ERROR = 'insan-group-pwa/Login/LOGIN_ERROR';

export const FACEBOOK_LOGIN = 'insan-group-pwa/Login/FACEBOOK_LOGIN';
export const GOOGLE_LOGIN = 'insan-group-pwa/Login/GOOGLE_LOGIN';
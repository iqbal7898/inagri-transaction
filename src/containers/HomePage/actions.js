/*
 * Home Actions
 *
 * Actions change things in your application
 * Since this boilerplate uses a uni-directional data flow, specifically redux,
 * we have these actions which are the only way your application interacts with
 * your application state. This guarantees that your state is up to date and nobody
 * messes it up weirdly somewhere.
 *
 * To add a new Action:
 * 1) Import your constant
 * 2) Add a function like this:
 *    export function yourAction(var) {
 *        return { type: YOUR_ACTION_CONSTANT, var: var }
 *    }
 */

import {
  DATE_START,
  DATE_END,
  LOAD_DATA,
  LOAD_DATA_SUCCESS,
  LOAD_DATA_ERROR,
} from './constants';


export function changeDateStart(dateStart) {
  return {
    type: DATE_START,
    dateStart
  };
}

export function changeDateEnd(dateEnd) {
  return {
    type: DATE_END,
    dateEnd
  };
}

export function loadData() {
  return {
    type: LOAD_DATA,
  };
}

export function dataLoaded(
  customerCount,
  customerTransaction,
  productTransaction,
  dailyOrder,
  dailyBuy,
  dailySell,
  bestSale,
) {
  return {
    type: LOAD_DATA_SUCCESS,
    customerCount,
    customerTransaction,
    productTransaction,
    dailyOrder,
    dailyBuy,
    dailySell,
    bestSale,
  };
}

export function dataLoadingError(error) {
  return {
    type: LOAD_DATA_ERROR,
    error,
  };
}

import { call, all, put, select, takeLatest } from 'redux-saga/effects';
import { request, getRequestURL } from 'utils/request';
import { TRANSACTION_MODEL } from 'utils/constants';
import { LOAD_DATA } from './constants';
import {
  dataLoaded,
  dataLoadingError,
} from './actions';
import {
  makeSelectDateStart,
  makeSelectDateEnd,
} from './selectors';
import { currencyFormatter } from "../../utils/helper";

function* getData() {
  // Select company from store
  try {
    const dateStart = yield select(makeSelectDateStart());
    const dateEnd = yield select(makeSelectDateEnd());
    const token = localStorage.getItem('token');
    // Call our request helper (see 'utils/request')
    console.log('dateStart', dateStart);

    let query = '';
    if(dateStart && dateEnd){
      query = `&start=${dateStart}&end=${dateEnd}`;
    } else if(dateStart) {
      query = `&start=${dateStart}`;
    }

    const [customerCount, customerTransaction, productTransaction, dailyOrder, dailyBuy, dailySell, bestSale] = yield all([
      call(request, `${getRequestURL(TRANSACTION_MODEL)}?choice=1${query}`, {
        method: "GET",
        headers: {
          "Content-Type": "application/json",
          Authorization: `Bearer ${token}`
        }
      }),
      call(request, `${getRequestURL(TRANSACTION_MODEL)}?choice=2&group_by=customer${query}`, {
        method: 'GET',
        headers: {
          "Content-Type": "application/json",
          Authorization: `Bearer ${token}`
        }
      }),
      call(request, `${getRequestURL(TRANSACTION_MODEL)}?choice=2&group_by=product${query}`, {
        method: 'GET',
        headers: {
          "Content-Type": "application/json",
          Authorization: `Bearer ${token}`
        }
      }),
      call(request, `${getRequestURL(TRANSACTION_MODEL)}?choice=3${query}`, {
        method: 'GET',
        headers: {
          "Content-Type": "application/json",
          Authorization: `Bearer ${token}`
        }
      }),
      call(request, `${getRequestURL(TRANSACTION_MODEL)}?choice=4&group_by=buy${query}`, {
        method: 'GET',
        headers: {
          "Content-Type": "application/json",
          Authorization: `Bearer ${token}`
        }
      }),
      call(request, `${getRequestURL(TRANSACTION_MODEL)}?choice=4&group_by=sell${query}`, {
        method: 'GET',
        headers: {
          "Content-Type": "application/json",
          Authorization: `Bearer ${token}`
        }
      }),
      call(request, `${getRequestURL(TRANSACTION_MODEL)}?choice=5${query}`, {
        method: 'GET',
        headers: {
          "Content-Type": "application/json",
          Authorization: `Bearer ${token}`
        }
      })
    ]);
    
    const customerTransactionList = customerTransaction.data.data
      .sort((a, b) => a.no < b.no ? 1 : -1);

    const productTransactionList = productTransaction.data.data
      .sort((a, b) => a.no < b.no ? 1 : -1);

    const dailyBuyList = dailyBuy.data.data.map((row, index) => {
      const dateSplit = row.date.split("-");
      const dateNew = new Date(parseInt(dateSplit[2]),parseInt(dateSplit[1])-1,parseInt(dateSplit[0]));
      return {
        no: index + 1,
        date: row.date,
        formatDate: dateNew,
        buy: row.buy,
        label: currencyFormatter.format(row.buy),
      }
    });

    const dailySellList = dailySell.data.data.map((row, index) => ({
      no: index + 1,
      date: row.date,
      sell: row.sell,
      label: currencyFormatter.format(row.sell),
    }));

    yield put(dataLoaded(
      customerCount.data.data.customer_count,
      customerTransactionList,
      productTransactionList,
      dailyOrder.data.data,
      dailyBuyList,
      dailySellList,
      bestSale.data.data
    ));
  } catch (err) {
    console.log('err.response.data', err)
    yield put(dataLoadingError(err));
  }
} 

// Individual exports for testing
export default function* homepageSaga() {
  // See example in containers/HomePage/saga.js
  yield takeLatest(LOAD_DATA, getData);
}

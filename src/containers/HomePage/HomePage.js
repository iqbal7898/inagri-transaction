/*
 * HomePage
 *
 * This is the first thing users see of our App, at the '/' route
 */

import React, { useState, useEffect, memo } from 'react';
import { Helmet } from 'react-helmet';
import { connect } from 'react-redux';
import { compose } from 'redux';
import PropTypes from 'prop-types';
import { 
  VictoryChart,
  VictoryLine,
  VictoryTooltip,
  createContainer
} from 'victory';
import * as Styles from './styles';
import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardHeader from '@material-ui/core/CardHeader';
import CardContent from '@material-ui/core/CardContent';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Typography from '@material-ui/core/Typography';
import InputLabel from '@material-ui/core/InputLabel';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';
import TextField from '@material-ui/core/TextField';
import DateFnsUtils from '@date-io/date-fns';
import * as jwt_decode from 'jwt-decode';
import { 
  DatePicker, 
  MuiPickersUtilsProvider 
} from "@material-ui/pickers";
import Button from '../../components/Button/Button';
import { useInjectSaga } from 'utils/injectSaga';
import { useInjectReducer } from 'utils/injectReducer';
import { currencyFormatter } from "../../utils/helper";
import { createStructuredSelector } from "reselect";
import reducer from './reducer';
import saga from './saga';
import {
  loadData,
  changeDateStart,
  changeDateEnd,
} from "./actions";
import {
  makeSelectCustomerCount,
  makeSelectCustomerTransaction,
  makeSelectProductTransaction,
  makeSelectDailyOrder,
  makeSelectDailyBuy,
  makeSelectDailySell,
  makeSelectBestSale,
  makeSelectDateStart,
  makeSelectDateEnd,
} from "./selectors";

const useStyles = makeStyles((theme) => ({
  root: {
    minWidth: 275,
    overflow: 'auto',
  },
  container: {
    maxHeight: 440,
  },
  bullet: {
    display: 'inline-block',
    margin: '0 2px',
    transform: 'scale(0.8)',
  },
  title: {
    fontSize: 14,
  },
  pos: {
    marginBottom: 12,
  },
  table: {
    minWidth: '30%',
  },
  formControl: {
    margin: theme.spacing(1),
    minWidth: 120,
  },
}));

export function HomePage({
  onLoadData,
  onChangeDateStart,
  onChangeDateEnd,
  customerCount,
  customerTransaction,
  productTransaction,
  dailyOrder,
  dailyBuy,
  dailySell,
  bestSale,
  dateStart,
  dateEnd,
  history
}) {
  useInjectReducer({ key: 'homepage', reducer });
  useInjectSaga({ key: 'homepage', saga });

  useEffect(() => {
    onLoadData();
  }, [onLoadData]);


  useEffect(() => {
    if (localStorage.getItem('token')) {
      const token = localStorage.getItem('token');
      var decoded = jwt_decode(token);
      var dateToken = new Date(decoded.exp * 1000);
      console.log('decode', dateToken.toUTCString());
      console.log('Date.now()', Date.now());
      console.log('difference', Date.now() - decoded.exp * 1000);
      if (Date.now() > decoded.exp * 1000) {
        localStorage.clear();
        history.push('/login');
      }
    }
  }, [history]);

  const [date, setDate] = useState(0);
  const [dateStartTemp, setDateStartTemp] = useState(0);
  const [dateEndTemp, setDateEndTemp] = useState(0);
  const handleChange = (event) => {
    const dateNow = new Date();
    if (event.target.value === 0) {
      setDate(event.target.value);
      onChangeDateStart(false);
    } else {
      setDate(event.target.value);
      dateNow.setDate(dateNow.getDate() - event.target.value);
      setDateStartTemp(dateNow);
      onChangeDateStart(`${dateNow.getDate()}-${dateNow.getMonth() + 1}-${dateNow.getFullYear()}`);
    }
    onLoadData()
  };

  const handleDateStartChange = (date) => {
    console.log('date', `${date.getDate()}-${date.getMonth() + 1}-${date.getFullYear()}`)
    setDateStartTemp(date)
    onChangeDateStart(`${date.getDate()}-${date.getMonth() + 1}-${date.getFullYear()}`);
    onLoadData()
  };

  const handleDateEndChange = (date) => {
    setDateEndTemp(date)
    onChangeDateEnd(`${date.getDate()}-${date.getMonth() + 1}-${date.getFullYear()}`);
    onLoadData()
  };

  function getDailyBuyMax(){
    let max = 0;
    dailyBuy && dailyBuy.map((row) => {
      if(max < row.buy){
        max = row.buy
      };
      return max;
    });
    console.log('buy max', max);
    return max;
  };

  function getDailySellMax() {
    let max = 0;
    dailySell && dailySell.map((row) => {
      if(max < row.sell){
        max = row.sell
      }
      return max;
    });
    console.log('sell max', max);
    return max;
  };

  const classes = useStyles();

  const VictoryZoomVoronoiContainer = createContainer("zoom", "voronoi");

  return (
    <div>
      <Helmet>
        <meta charset="utf-8" />
        <title>Inagri - Supplier Sayur Buah Sembako Online</title>
        <meta
          name="description"
          content="Inagri - Supplier Sayur Buah Sembako Online"
        />
      </Helmet>

      <Styles.GlobalContainer>
        <Styles.top>
          <FormControl className={classes.formControl}>
            <InputLabel id="demo-simple-select-label">Waktu</InputLabel>
            <Select
              labelId="demo-simple-select-label"
              id="demo-simple-select"
              value={date}
              onChange={handleChange}
            >
              <MenuItem value={7}>Satu Minggu Terakhir</MenuItem>
              <MenuItem value={14}>Dua Minggu Terakhir</MenuItem>
              <MenuItem value={30}>Satu Bulan Terakhir</MenuItem>
              <MenuItem value={0}>Semua</MenuItem>
            </Select>
          </FormControl>
          <MuiPickersUtilsProvider utils={DateFnsUtils}>
            <DatePicker
              label="Tanggal Mulai"
              value={dateStartTemp}
              onChange={(newValue) => handleDateStartChange(newValue)}
              renderInput={(props) => <TextField {...props} />}
            />
            <DatePicker
              label="Tanggal Selesai"
              value={dateEndTemp}
              onChange={(newValue) => handleDateEndChange(newValue)}
              renderInput={(props) => <TextField {...props} />}
            />
          </MuiPickersUtilsProvider>

          <Button
              variant="outline-secondary"
              size="lg"
              onClick={() => {
                localStorage.clear();
                history.push('/login')
              }}
            >
              <Styles.LogOutButton>
                KELUAR AKUN
              </Styles.LogOutButton>
            </Button>
        </Styles.top>
        <Styles.Row1>
          <Styles.column1>
            <Card className={classes.root}>
              <CardHeader
                title="Jumlah Customer"
              />
              <CardContent>
                <Typography className={classes.title} color="textSecondary" gutterBottom>
                  {customerCount}
                </Typography>
              </CardContent>
            </Card>

            <Card className={classes.root}>
              <CardHeader
                title="Komoditas yg sering dibeli"
              />
              <CardContent>
                <Typography className={classes.title} color="textSecondary" gutterBottom>
                  Nama Barang: {bestSale.product}
                </Typography>

                <Typography className={classes.title} color="textSecondary" gutterBottom>
                  Jumlah : {bestSale.no}
                </Typography>

                <Typography className={classes.title} color="textSecondary" gutterBottom>
                  Penjualan : {currencyFormatter.format(bestSale.sell)}
                </Typography>
              </CardContent>
            </Card>
          </Styles.column1>

          <TableContainer className={classes.container}>
            <Table className={classes.table} stickyHeader size="small" aria-label="simple table">
              <TableHead>
              <Typography className={classes.title} variant="h6" id="tableTitle" component="div">
                Transaksi Pembeli
              </Typography>
                <TableRow>
                  <TableCell>Pembeli</TableCell>
                  <TableCell align="right">Jumlah Transaksi</TableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                {customerTransaction && customerTransaction.map((row) => (
                  <TableRow hover key={row.name}>
                    <TableCell component="th" scope="row">
                      {row.customer}
                    </TableCell>
                    <TableCell align="right">{row.no}</TableCell>
                  </TableRow>
                ))}
              </TableBody>
            </Table>
          </TableContainer>

          <TableContainer className={classes.container}>
            <Table className={classes.table} stickyHeader size="small" aria-label="simple table">
              <TableHead>
              <Typography className={classes.title} variant="h6" id="tableTitle" component="div">
                Transaksi Barang
              </Typography>
                <TableRow>
                  <TableCell>Barang</TableCell>
                  <TableCell align="right">Jumlah Transaksi</TableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                {productTransaction && productTransaction.map((row) => (
                  <TableRow hover key={row.name}>
                    <TableCell component="th" scope="row">
                      {row.product}
                    </TableCell>
                    <TableCell align="right">{row.no}</TableCell>
                  </TableRow>
                ))}
              </TableBody>
            </Table>
          </TableContainer>
        </Styles.Row1>

        <Styles.Row2>
          <Styles.ChartContainer>
            {console.log('getDailyBuyMax', getDailyBuyMax())}
          <VictoryChart
            containerComponent={
              <VictoryZoomVoronoiContainer 
                mouseFollowTooltips
                voronoiDimension="x"
                label={d => `${d.label}`}
              />
            }
            width={1200}
            height={440}
            // viewBox="0 0 1100 440"
            padding={{top: 50, left: 100, right: 50, bottom: 50}}
            // domain={{x: [0, dailyBuy.length], y: [0, getDailyBuyMax()]}}
            // containerComponent={<VictoryZoomContainer zoomDomain={{x: [0, dailyBuy.length], y: [0, getDailyBuyMax()]}}/>}
          >
            <VictoryLine
              labelComponent={<VictoryTooltip />}
              style={{
                data: { stroke: "#02B875" }
              }}
              data={dailyBuy}
              x="formatDate"
              y="buy"
            />
          </VictoryChart>
        </Styles.ChartContainer>
        <Styles.column1>
          <TableContainer className={classes.container}>
            <Table className={classes.table} stickyHeader size="small" aria-label="simple table">
              <TableHead>
              <Typography className={classes.title} variant="h6" id="tableTitle" component="div">
                Order Harian
              </Typography>
                <TableRow>
                  <TableCell>No</TableCell>
                  <TableCell>Tanggal</TableCell>
                  <TableCell align="right">Pembelian</TableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                {dailyBuy && dailyBuy.map((row) => (
                  <TableRow hover key={row.name}>
                  <TableCell component="th" scope="row">
                    {row.no}
                  </TableCell>
                    <TableCell component="th" scope="row">
                      {row.date}
                    </TableCell>
                    <TableCell align="right">{currencyFormatter.format(row.buy)}</TableCell>
                  </TableRow>
                ))}
              </TableBody>
            </Table>
          </TableContainer>
        </Styles.column1>
        </Styles.Row2>
        <Styles.Row2>
          <Styles.ChartContainer>
            {console.log('getDailySellMax', getDailySellMax())}
          <VictoryChart 
            containerComponent={
              <VictoryZoomVoronoiContainer 
                mouseFollowTooltips
                voronoiDimension="x"
                label={d => `${d.label}`} 
              />
            }
            width={1200}
            height={440}
            // viewBox={"0 0 1100 440"}
            padding={{top: 50, left: 100, right: 50, bottom: 50}}
            // domain={{x: [0, dailySell.length], y: [0, getDailySellMax()]}}
            // containerComponent={<VictoryZoomContainer zoomDomain={{x: [0, dailySell.length], y: [0, getDailySellMax()]}}/>}
          >
            <VictoryLine
              labelComponent={<VictoryTooltip />}
              style={{
                data: { stroke: "#02B875" }
              }}
              data={dailySell}
              x="no"
              y="sell"
            />
          </VictoryChart>
        </Styles.ChartContainer>

        <Styles.column1>
          <TableContainer className={classes.container}>
            <Table className={classes.table} stickyHeader size="small" aria-label="simple table">
              <TableHead>
              <Typography className={classes.title} variant="h6" id="tableTitle" component="div">
                Order Harian
              </Typography>
                <TableRow>
                  <TableCell>No</TableCell>
                  <TableCell>Tanggal</TableCell>
                  <TableCell align="right">Penjualan</TableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                {dailySell && dailySell.map((row) => (
                  <TableRow hover key={row.name}>
                  <TableCell component="th" scope="row">
                    {row.no}
                  </TableCell>
                    <TableCell component="th" scope="row">
                      {row.date}
                    </TableCell>
                    <TableCell align="right">{currencyFormatter.format(row.sell)}</TableCell>
                  </TableRow>
                ))}
              </TableBody>
            </Table>
          </TableContainer>

        </Styles.column1>
        </Styles.Row2>
      </Styles.GlobalContainer>
    </div>
  );
}

HomePage.propTypes = {
  history: PropTypes.object,
  location: PropTypes.object,
  onLoadData: PropTypes.func,
};

const mapStateToProps = createStructuredSelector({
  customerCount: makeSelectCustomerCount(),
  customerTransaction: makeSelectCustomerTransaction(),
  productTransaction: makeSelectProductTransaction(),
  dailyOrder: makeSelectDailyOrder(),
  dailyBuy: makeSelectDailyBuy(),
  dailySell: makeSelectDailySell(),
  bestSale: makeSelectBestSale(),
  dateStart: makeSelectDateStart(),
  dateEnd: makeSelectDateEnd(),
});

function mapDispatchToProps(dispatch) {
  return {
    onLoadData: (id) => dispatch(loadData()),
    onChangeDateStart: (dateStart) => dispatch(changeDateStart(dateStart)),
    onChangeDateEnd: (dateEnd) => dispatch(changeDateEnd(dateEnd)),
  };
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

export default compose(
  withConnect,
  memo,
)(HomePage);

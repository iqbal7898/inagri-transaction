import { createSelector } from 'reselect';
import { initialState } from './reducer';

/**
 * Direct selector to the homepage state domain
 */

export const selectHomepage = state => state.homepage || initialState;

export const makeSelectLoading = () =>
  createSelector(
    selectHomepage,
    dataState => dataState.loading,
  );

export const makeSelectError = () =>
  createSelector(
    selectHomepage,
    dataState => dataState.error,
  );

//DATA
export const makeSelectCustomerCount = () =>
  createSelector(
    selectHomepage,
    dataState => dataState.data.customerCount,
  );

export const makeSelectCustomerTransaction = () =>
  createSelector(
    selectHomepage,
    dataState => dataState.data.customerTransaction,
  );

export const makeSelectProductTransaction = () =>
  createSelector(
    selectHomepage,
    dataState => dataState.data.productTransaction,
  );

export const makeSelectDailyOrder = () =>
  createSelector(
    selectHomepage,
    dataState => dataState.data.dailyOrder,
  );

export const makeSelectDailyBuy = () =>
  createSelector(
    selectHomepage,
    dataState => dataState.data.dailyBuy,
  );

export const makeSelectDailySell = () =>
  createSelector(
    selectHomepage,
    dataState => dataState.data.dailySell,
  );

export const makeSelectBestSale = () =>
  createSelector(
    selectHomepage,
    dataState => dataState.data.bestSale,
  );

export const makeSelectDateStart = () =>
  createSelector(
    selectHomepage,
    dataState => dataState.data.dateStart,
  );

export const makeSelectDateEnd = () =>
  createSelector(
    selectHomepage,
    dataState => dataState.data.dateEnd,
  );
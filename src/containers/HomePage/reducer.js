/*
 * HomeReducer
 *
 * The reducer takes care of our data. Using actions, we can
 * update our application state. To add a new action,
 * add it to the switch statement in the reducer function
 *
 */

import produce from 'immer';
import {
  DATE_START,
  DATE_END,
  LOAD_DATA,
  LOAD_DATA_SUCCESS,
  LOAD_DATA_ERROR,
} from './constants';

// The initial state of the App
export const initialState = {
  loading: true,
  error: {
    tableData: false,
  },
  data: {
    customerCount: false,
    customerTransaction: false,
    productTransaction: false,
    dailyOrder: false,
    dailyBuy: false,
    dailySell: false,
    bestSale: false,
    dateStart: false,
    dateEnd: false
  },
  search: {
    general: ""
  },
};

/* eslint-disable default-case, no-param-reassign */
const HomepageReducer = (state = initialState, action) =>
  produce(state, draft => {
    switch (action.type) {
      case DATE_START:
        draft.data.dateStart = action.dateStart;
        break;

      case DATE_END:
        draft.data.dateEnd = action.dateEnd;
        break;
      
      case LOAD_DATA:
        draft.loading = true;
        draft.error = initialState.error;
        draft.data.tableData = false;
        break;

      case LOAD_DATA_SUCCESS:
        draft.data.tableData = action.tableData;
        draft.data.customerCount = action.customerCount;
        draft.data.customerTransaction = action.customerTransaction;
        draft.data.productTransaction = action.productTransaction;
        draft.data.dailyOrder = action.dailyOrder;
        draft.data.dailyBuy = action.dailyBuy;
        draft.data.dailySell = action.dailySell;
        draft.data.bestSale = action.bestSale;
        draft.loading = false;
        break;

      case LOAD_DATA_ERROR:
        draft.error.tableData = action.error;
        draft.loading = false;
        break;
    }
  });

export default HomepageReducer;

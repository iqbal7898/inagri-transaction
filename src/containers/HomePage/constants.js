/*
 * HomeConstants
 * Each action has a corresponding type, which the reducer knows and picks up on.
 * To avoid weird typos between the reducer and the actions, we save them as
 * constants here. We prefix them with 'yourproject/YourComponent' so we avoid
 * reducers accidentally picking up actions they shouldn't.
 *
 * Follow this format:
 * export const YOUR_ACTION_CONSTANT = 'yourproject/YourContainer/YOUR_ACTION_CONSTANT';
 */

export const DEFAULT_ACTION = 'inagri-transaction/Homepage/DEFAULT_ACTION';
export const DATE_START = 'inagri-transaction/Homepage/DATE_START';
export const DATE_END = 'inagri-transaction/Homepage/DATE_END';
export const LOAD_DATA = 'inagri-transaction/Homepage/LOAD_DATA';
export const LOAD_DATA_SUCCESS = 'inagri-transaction/Homepage/LOAD_DATA_SUCCESS';
export const LOAD_DATA_ERROR = 'inagri-transaction/Homepage/LOAD_DATA_ERROR';
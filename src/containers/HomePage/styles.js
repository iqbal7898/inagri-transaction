import styled from 'styled-components';

export const GlobalContainer = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  width: 100%;
  margin: 15px;
`;

export const LogOutButton = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  height: 28px;
  min-width: 50px;
  padding: 0 12.4444444444px;
  font-family: 'Baloo 2';
  font-size: 0.75em;
  color: #ffffff;
  border-color: 2px solid rgb(170, 170, 170);
`;

export const top = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: flex-start;
  align-items: center;
  width: 100%;
  margin: 15px;
  height: 44px;
`;

export const Row1 = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  align-items: flex-start;
  width: 100%;
  margin: 15px;
  height: 440px;
`;

export const column1 = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  align-items: flex-start;
  width: 30%;
  margin: 15px;
  height: 440px;
`;

export const Row2 = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  width: 100%;
  margin: 15px;
`;

export const ChartContainer = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  width: 70%;
  box-shadow: rgba(57, 63, 72, 0.3) 0px 2px 6px;
  background-color: #ffffff;
  border-radius: 5px;
`;
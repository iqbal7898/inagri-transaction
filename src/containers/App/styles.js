import styled from 'styled-components';

export const Static = styled.div`
  position: fixed;
  z-index: 99;
  width: 100%;
  justify-content: center;
  display: flex;
  box-shadow: rgba(57, 63, 72, 0.3) 0px 1px 3px;
`;

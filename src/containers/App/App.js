/**
 *
 * App
 *
 * This component is the skeleton around the actual pages, and should only
 * contain code that should be seen on all pages. (e.g. navigation bar)
 */

import React, { } from 'react';
import { Helmet } from 'react-helmet';
import styled from 'styled-components';
import {
  Switch,
  Route,
  BrowserRouter as Router,
  withRouter,
} from 'react-router-dom';

import Home from '../HomePage/Loadable';
import Login from '../Login/Loadable';
import PropTypes from 'prop-types';

const AppWrapper = styled.div`
  display: flex;
  width: 100%;
  margin: 0 auto;
  flex: 1;
  min-height: 100%;
  flex-direction: column;
`;

function App({ location }) {
  return (
    <Router>
      <AppWrapper>
        <Helmet titleTemplate="%s - Inagri " defaultTitle="Inagri">
          <meta charSet="utf-8" />
          <title>Inagri - Supplier Sayur Buah Sembako Online</title>
          <meta
            name="description"
            content="Inagri - Supplier Sayur Buah Sembako Online"
          />
        </Helmet>
        <Switch>
          {/* Home */}
          <Route exact path="/" component={Home} />
          <Route exact path="/login" component={Login} />
        </Switch>
      </AppWrapper>
    </Router>
  );
}

App.propTypes = {
  location: PropTypes.object,
};

export default withRouter(App);

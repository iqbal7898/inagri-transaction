import { css } from 'styled-components';

export const buttonStyles = css`
  display: inline-block;
  text-align: center;
  box-sizing: border-box;
  padding: 0.25em 2em;
  text-decoration: none;
  border-radius: 5px;
  -webkit-font-smoothing: antialiased;
  -webkit-touch-callout: none;
  user-select: none;
  cursor: pointer;
  outline: 0;
  font-family: 'Baloo 2';
  font-weight: bold;
  font-size: 18px;
  color: ${props => (props.white ? '#86c15a' : '#ffffff')};
  background-color: ${props => (props.white ? '#ffffff' : '#86c15a')};

  &:active {
    background: #ffffff;
    color: #aeea00;
  }

  @media (max-width: 800px) {
    font-size: 15px;
    padding: 0.2em 1em;
  }
`;

export const largeButtonStyles = css`
  display: inline-block;
  text-align: center;
  box-sizing: border-box;
  padding: 0.25em 2em;
  text-decoration: none;
  border-radius: 10px;
  -webkit-font-smoothing: antialiased;
  -webkit-touch-callout: none;
  user-select: none;
  cursor: pointer;
  outline: 0;
  font-family: 'Baloo 2';
  font-weight: bold;
  font-size: 28px;
  color: #ffffff;
  background-color: #07c160;

  &:active {
    background: #ffffff;
    color: #07c160;
  }

  @media (max-width: 800px) {
    font-size: 16px;
    padding: 0.12em 1em;
  }
`;

export const regularButtonStyles = css`
  display: inline-block;
  text-align: center;
  box-sizing: border-box;
  padding: 0.25em 2em;
  text-decoration: none;
  border-radius: 5px;
  -webkit-font-smoothing: antialiased;
  -webkit-touch-callout: none;
  user-select: none;
  cursor: pointer;
  outline: 0;
  font-family: 'Baloo 2';
  font-weight: bold;
  font-size: 20px;
  color: #ffffff;
  background-color: #07c160;

  &:active {
    background: #ffffff;
    color: #07c160;
  }

  @media (max-width: 800px) {
    font-size: 16px;
    padding: 0.12em 1em;
  }
`;

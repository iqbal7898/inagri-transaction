import styled from 'styled-components';

import { buttonStyles, largeButtonStyles } from './buttonStyles';

export const A = styled.a`
  ${buttonStyles};
`;

export const LargeA = styled.a`
  ${largeButtonStyles};
`;

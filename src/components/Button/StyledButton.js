import styled from 'styled-components';

import {
  buttonStyles,
  largeButtonStyles,
  regularButtonStyles,
} from './buttonStyles';

export const StyledButton = styled.button`
  ${buttonStyles};
`;

export const LargeStyledButton = styled.button`
  ${largeButtonStyles};
`;

export const RegularStyledButton = styled.button`
  ${regularButtonStyles};
`;

import React from 'react';
import { render } from 'react-testing-library';

import { StyledButton, LargeStyledButton } from '../StyledButton';

describe('<StyledButton />', () => {
  it('should render an <button> tag', () => {
    const { container } = render(<StyledButton />);
    expect(container.querySelector('button')).not.toBeNull();
  });

  it('should have a class attribute', () => {
    const { container } = render(<StyledButton />);
    expect(container.querySelector('button').hasAttribute('class')).toBe(true);
  });

  it('should adopt a valid attribute', () => {
    const id = 'test';
    const { container } = render(<StyledButton id={id} />);
    expect(container.querySelector('button').id).toEqual(id);
  });

  it('should not adopt an invalid attribute', () => {
    const { container } = render(<StyledButton attribute="test" />);
    expect(container.querySelector('button[attribute="test"]')).toBeNull();
  });
});

describe('<LargeStyledButton />', () => {
  it('should render an <button> tag', () => {
    const { container } = render(<LargeStyledButton />);
    expect(container.querySelector('button')).not.toBeNull();
  });

  it('should have a class attribute', () => {
    const { container } = render(<LargeStyledButton />);
    expect(container.querySelector('button').hasAttribute('class')).toBe(true);
  });

  it('should adopt a valid attribute', () => {
    const id = 'test';
    const { container } = render(<LargeStyledButton id={id} />);
    expect(container.querySelector('button').id).toEqual(id);
  });

  it('should not adopt an invalid attribute', () => {
    const { container } = render(<LargeStyledButton attribute="test" />);
    expect(container.querySelector('button[attribute="test"]')).toBeNull();
  });
});

/**
 *
 * Button.js
 *
 * A common button, if you pass it a prop "route" it'll render a link to a react-router route
 * otherwise it'll render a link with an onclick
 */

import React, { Children } from 'react';
import PropTypes from 'prop-types';

import { A, LargeA } from './A';
import {
  StyledButton,
  LargeStyledButton,
  RegularStyledButton,
} from './StyledButton';

function Button(props) {
  // Render an anchor tag
  let button =
    props.size === 'large' ? (
      <LargeA href={props.href} onClick={props.onClick}>
        {Children.toArray(props.children)}
      </LargeA>
    ) : (
      <A href={props.href} onClick={props.onClick}>
        {Children.toArray(props.children)}
      </A>
    );
  // If the Button has a handleRoute prop, we want to render a button
  if (props.handleRoute) {
    if (props.size === 'large') {
      button = (
        <LargeStyledButton onClick={props.handleRoute}>
          {Children.toArray(props.children)}
        </LargeStyledButton>
      );
    } else if (props.size === 'regular') {
      button = (
        <RegularStyledButton onClick={props.handleRoute}>
          {Children.toArray(props.children)}
        </RegularStyledButton>
      );
    } else {
      button = (
        <StyledButton onClick={props.handleRoute}>
          {Children.toArray(props.children)}
        </StyledButton>
      );
    }
  }

  return button;
}

Button.propTypes = {
  size: PropTypes.string,
  handleRoute: PropTypes.func,
  href: PropTypes.string,
  onClick: PropTypes.func,
  children: PropTypes.node.isRequired,
};

export default Button;

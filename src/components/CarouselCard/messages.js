/*
 * HomePage Messages
 *
 * This contains all the text for the HomePage component.
 */
import { defineMessages } from 'react-intl';

export const scope = 'inaventory-marketplace.components.Card';

export default defineMessages({
  area: {
    id: `${scope}.area`,
    defaultMessage: 'Luas',
  },
  verified: {
    id: `${scope}.verified`,
    defaultMessage: 'Terverifikasi',
  },
  unverified: {
    id: `${scope}.unverified`,
    defaultMessage: 'Belum Terverifikasi',
  },
  month: {
    id: `${scope}.month`,
    defaultMessage: 'Bulan',
  },
});

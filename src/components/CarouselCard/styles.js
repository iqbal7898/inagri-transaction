import styled from 'styled-components';

export const Box = styled.div`
  width: 420px;
  display: block;
  height: 252px;
  padding-left: 11px;
  padding-right: 11px;
  min-height: 0px;
  min-width: 0px; 
`;

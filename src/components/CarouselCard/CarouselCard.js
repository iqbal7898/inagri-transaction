import React from 'react';
import PropTypes from 'prop-types';
import Slider from 'react-slick';
import {
  Box,
} from './styles';
import Img from '../Img/Img';

function CarouselCard(props) {
  var settings = {
    dots: true,
    infinite: true,
    speed: 400,
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: false,
  };
  
  return (
    <Box>
      <Slider {...settings}>
        <div>
          <Img
            className="sampleImage"
            src={props.image}
            alt="sample image"
            height="222px"
            widht="425px"
          />
        </div>
        <div>
          <Img
            className="sampleImage"
            src={props.image}
            alt="sample image"
            height="222px"
            widht="425px"
          />
        </div>
      </Slider>
    </Box>
  );
}

CarouselCard.defaultProps = {
  verified: false,
};

CarouselCard.propTypes = {
  image: PropTypes.string.isRequired,
};

export default CarouselCard;

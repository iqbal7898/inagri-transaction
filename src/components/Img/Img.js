/**
 *
 * Img.js
 *
 * Renders an image, enforcing the usage of the alt="" tag
 */

import React from 'react';
import PropTypes from 'prop-types';

function Img(props) {
  return (
    <img
      id={props.id}
      className={props.className}
      src={props.src}
      alt={props.alt}
      height={props.height}
      width={props.width}
      onClick={props.onClick}
    />
  );
}

// We require the use of src and alt, only enforced by react in dev mode
Img.propTypes = {
  id: PropTypes.string,
  src: PropTypes.oneOfType([PropTypes.string, PropTypes.object]).isRequired,
  alt: PropTypes.string.isRequired,
  className: PropTypes.string,
  height: PropTypes.string,
  width: PropTypes.string,
};

export default Img;

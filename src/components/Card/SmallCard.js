import React from 'react';
import PropTypes from 'prop-types';
import {
  SMBox,
  Image,
  SMTitle,
  SMRightTextBox,
  SMPrice,
  SMDetailPrice,
  SMPriceContainer,
} from './styles';
import { currencyFormatter } from "../../utils/helper";

function SmallCard(props) {
  return (
    <SMBox onClick={props.onClickCard}>
      <Image
        className="sampleImage"
        src={props.image}
        alt="sample image"
        height="50px"
        widht="50px"
      />
      <SMRightTextBox>
        <SMTitle>{props.title}</SMTitle>
        {/* <ContentText>{props.detail}</ContentText> */}
        <SMPriceContainer>
          <SMDetailPrice>
            {props.goods.amount} @ {currencyFormatter.format(props.goods.discount ? props.goods.discount : props.price)}/{props.minQuantity} {props.unit}
          </SMDetailPrice>
          <SMPrice>
            Total: {currencyFormatter.format((props.goods.discount ? props.goods.discount : props.price) * props.goods.amount)}
          </SMPrice>
        </SMPriceContainer>
      </SMRightTextBox>
    </SMBox>
  );
}

SmallCard.defaultProps = {
  verified: false,
};

SmallCard.propTypes = {
  title: PropTypes.oneOfType([
    PropTypes.string.isRequired,
    PropTypes.node.isRequired,
  ]).isRequired,
  address: PropTypes.string,
  area: PropTypes.oneOfType([PropTypes.string, PropTypes.node]),
  verified: PropTypes.bool,
  price: PropTypes.number.isRequired,
  period: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.node,
  ]),
  image: PropTypes.string.isRequired,
};

export default SmallCard;

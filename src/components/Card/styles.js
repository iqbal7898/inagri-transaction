import styled from 'styled-components';

export const Box = styled.div`
  display: -webkit-box;
  display: -ms-flexbox;
  display: flex;
  -webkit-box-orient: vertical;
  -webkit-box-direction: normal;
      -ms-flex-direction: column;
  flex-direction: column;
  -webkit-box-pack: justify;
      -ms-flex-pack: justify;
  justify-content: space-between;
  width: calc(50% - 0.5em);
  height: 328px;
  -webkit-box-shadow: 0px 3px 3px rgba(0, 0, 0, 0.16);
  box-shadow: 0px 3px 3px rgba(0, 0, 0, 0.16);
  margin-right: 0.25em;
  margin-left: 0.25em;
  margin-bottom: 0.25em;
  background-color: #ffffff;
  border-radius: 5px;
`;

export const ImageContainer = styled.div`
  display: -webkit-box;
  display: -ms-flexbox;
  display: flex;
  -webkit-box-orient: vertical;
  -webkit-box-direction: normal;
      -ms-flex-direction: column;
  flex-direction: column;
  -webkit-box-pack: justify;
      -ms-flex-pack: justify;
  justify-content: space-between;
  -webkit-box-align: end;
      -ms-flex-align: end;
  align-items: flex-end;
  height: 220px;
`;

export const SaleContainer = styled.div`
  display: -webkit-box;
  display: -ms-flexbox;
  display: flex;
  -webkit-box-orient: horizontal;
  -webkit-box-direction: normal;
      -ms-flex-direction: row;
  flex-direction: row;
  -webkit-box-pack: start;
      -ms-flex-pack: start;
  justify-content: flex-start;
  -webkit-box-align: center;
      -ms-flex-align: center;
  align-items: center;
  width: 100%;
  height: 35px;
`;

export const Sale = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  width: 20%;
  height: 20px;  
  top: 10px;
  right: 10px;
  margin-left: 12px;
  font-family: 'Baloo 2';
  font-size: 0.625em;
  color: rgb(255, 255, 255);
  background-color: #FF0000;
  border-color: #FF0000;
  border-radius: 5px;
`;

export const CartBox = styled.div`
  display: flex;
  flex-direction: row;
  width: 100%;
  height: 128px;
  box-shadow: 0px 3px 3px rgba(0, 0, 0, 0.16);
  padding-top: 14.79px;
  padding-bottom: 14.03px;
  padding-left: 24.26px;
  padding-right: 21.74px;
  background-color: #ffffff;
`;

export const SMBox = styled.div`
  display: flex;
  flex-direction: row;
  width: 100%;
  height: 116px;
  padding-top: 14.79px;
  padding-bottom: 14.03px;
  background-color: #ffffff;
`;

export const ImageComponent = styled.div`
  border-radius: 5px;
  height: 160px;
  width: 100%;
`;

export const Image = styled.img`
  border-radius: 5px;
`;

export const CartImage = styled.img`
  border-radius: 10px;
  max-height: 100px;
  max-width: 100px;
`;

export const TextBox = styled.div`
  padding-top: 11.03px;
  display: flex;
  flex-direction: column;
  align-items: flex-start;
  width: 100%;
`;

export const Title = styled.span`
  font-family:'Baloo 2';
  font-size: 1rem;
  font-weight: 700;
  font-stretch: normal;
  font-style: normal;
  line-height: 1.3;
  letter-spacing: normal;
  text-align: left;
  color: #212121;
`;

export const SMTitle = styled.span`
  margin: 0 0.5rem;
  font-family:'Baloo 2';
  font-size: 17px;
  font-weight: 700;
  font-stretch: normal;
  font-style: normal;
  line-height: 1.3;
  letter-spacing: normal;
  text-align: left;
  color: #212121;
`;

export const CartTitle = styled.span`
  font-family:'Baloo 2';
  font-size: 17px;
  font-weight: 700;
  font-stretch: normal;
  font-style: normal;
  line-height: 1.3;
  letter-spacing: normal;
  text-align: left;
  color: #212121;
`;

export const ContentText = styled.span`
  font-family: 'Baloo 2';
  font-size: 12px;
  color: #707070;
`;

export const RightTextBox = styled.div`
  display: -webkit-box;
  display: -ms-flexbox;
  display: flex;
  flex-direction: column;
  justify-content: flex-start;
  width: 100%;
  height: 100px;
  padding-left: 12px;
`;

export const SMRightTextBox = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  align-items: flex-start;
  width: 100%;
  height: 80px;
  margin-left: 20px;
`;

export const CartRightTextBox = styled.div`
  padding-left: 24.27px;
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  width: 100%;
`;

export const DiscountPrice = styled.span`
  font-family:'Baloo 2';
  font-size: 0.75rem;
  font-weight: 400;
  font-stretch: normal;
  font-style: normal;
  line-height: 1.36;
  letter-spacing: normal;
  text-align: left;
  color: rgba(86, 100, 86, 0.5);
  text-decoration: line-through;
`;

export const SavePrice = styled.span`
  font-family:'Baloo 2';
  font-size: 0.875rem;
  font-weight: 400;
  font-stretch: normal;
  font-style: normal;
  line-height: 1.36;
  letter-spacing: normal;
  text-align: left;
  color: #FF0000;
`;

export const Price = styled.span`
  font-family:'Baloo 2';
  font-size: 1rem;
  font-weight: 400;
  font-stretch: normal;
  font-style: normal;
  line-height: 1.36;
  letter-spacing: normal;
  text-align: left;
  color: #212121;
`;

export const SMPrice = styled.span`
  margin: 0.5rem;
  font-family:'Baloo 2';
  font-size: 15px;
  font-weight: 400;
  font-stretch: normal;
  font-style: normal;
  line-height: 1.36;
  letter-spacing: normal;
  text-align: left;
  color: #212121;
`;

export const CartDiscountPrice = styled.span`
  font-family:'Baloo 2';
  font-size: 15px;
  font-weight: 400;
  font-stretch: normal;
  font-style: normal;
  line-height: 1.36;
  letter-spacing: normal;
  text-align: left;
  color: rgba(86, 100, 86, 0.5);
  text-decoration: line-through;
`;

export const CartPrice = styled.span`
  font-family:'Baloo 2';
  font-size: 15px;
  font-weight: 400;
  font-stretch: normal;
  font-style: normal;
  line-height: 1.36;
  letter-spacing: normal;
  text-align: left;
  color: #212121;
`;

export const SMDetailPrice = styled.span`
  margin: 0 0.5rem;
  font-family:'Baloo 2';
  font-size: 15px;
  font-weight: 400;
  font-stretch: normal;
  font-style: normal;
  line-height: 1.36;
  letter-spacing: normal;
  text-align: left;
  color: #AAAAAA;
`;

export const PriceContainer = styled.div`
  display: -webkit-box;
  display: -ms-flexbox;
  display: flex;
  -webkit-box-orient: horizontal;
  -webkit-box-direction: normal;
      -ms-flex-direction: row;
  flex-direction: row;
  -webkit-box-pack: start;
      -ms-flex-pack: start;
  justify-content: flex-start;
  width: 100%;
  height: 28px;
`;

export const DiscountPriceContainer = styled.div`
  display: -webkit-box;
  display: -ms-flexbox;
  display: flex;
  -webkit-box-orient: vertical;
  -webkit-box-direction: normal;
      -ms-flex-direction: column;
  flex-direction: column;
  justify-content: flex-start;
  width: 100%;
  height: 56px;
`;

export const CartPriceContainer = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: flex-start;
  width: 100%;
  height: 56px;
`;

export const DiscountCartPriceContainer = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: flex-start;
  width: 100%;
  height: 84px;
`;

export const SMPriceContainer = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: flex-start;
  width: 100%;
  height: 28px;
`;

export const PriceButton = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  width: calc(100% - 1rem);
  height: 38px;
  font-family: 'Baloo 2';
  font-size: 0.75em;
  color: rgb(255, 255, 255);
  background-color: rgb(134, 193, 90);
  border-color: rgb(134, 193, 90);
  border-radius: 5px;
  margin-bottom: 0.5rem;
  margin-left: 0.5rem;
  margin-right: 0.5rem;
`;

export const CartPriceButton = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  width: 100px;
  height: 28px;
  font-family: 'Baloo 2';
  font-size: 0.75em;
  color: rgb(255, 255, 255);
  background-color: rgb(134, 193, 90);
  border-color: rgb(134, 193, 90);
  border-radius: 15px;  
`;

export const AmountContainer = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  width: calc(100% - 1rem);
  height: 38px;
  margin-bottom: 0.5rem;
  margin-left: 0.5rem;
  margin-right: 0.5rem;
`;

export const CartAmountContainer = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  width: 108px;
  height: 28px;
`;

export const NominalAmount = styled.span`
  margin: 0.5rem;
  font-family:'Baloo 2';
  font-size: 15px;
  font-weight: 400;
  font-stretch: normal;
  font-style: normal;
  line-height: 1.36;
  letter-spacing: normal;
  text-align: left;
  color: #212121;
`;
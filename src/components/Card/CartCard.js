import React from 'react';
import PropTypes from 'prop-types';
import {
  CartBox,
  CartImage,
  CartTitle,
  CartRightTextBox,
  CartPrice,
  CartDiscountPrice,
  CartPriceContainer,
  CartPriceButton,
  CartAmountContainer
} from './styles';
import { currencyFormatter } from "../../utils/helper";

function Card(props) {
  return (
    <CartBox onClick={props.onClickCard}>
      <CartImage
        className="sampleImage"
        src={props.image}
        alt="sample image"
        height="100px"
        widht="100px"
      />
      <CartRightTextBox>
        <CartTitle>{props.title}</CartTitle>
        {/* <ContentText>{props.detail}</ContentText> */}
        {props.goods.discount ? (<CartPriceContainer>
          <CartDiscountPrice>
            {currencyFormatter.format(props.price)}/{props.minQuantity} {props.unit}
          </CartDiscountPrice>
          <CartPrice>
            {currencyFormatter.format(props.goods.discount)}/{props.minQuantity} {props.unit}
          </CartPrice>
        </CartPriceContainer>) : (<CartPriceContainer>
          <CartPrice>
            {currencyFormatter.format(props.price)}/{props.minQuantity} {props.unit}
          </CartPrice>
        </CartPriceContainer>)}
        {props.cartList.find(row => row.goodsId === props.goods.id) ? (
          <CartAmountContainer>
            <CartImage
              className="sampleImage"
              src={require('../../assets/images/minus.svg')}
              alt="sample image"
              height="28px"
              widht="28px"
              onClick={props.onClickReduceItems}
            />
            <span>{props.cartList.find(row => row.goodsId === props.goods.id).items}</span>
            <CartImage
              className="sampleImage"
              src={require('../../assets/images/plus.svg')}
              alt="sample image"
              height="28px"
              widht="28px"
              onClick={props.onClickAddItems}
            />
          </CartAmountContainer>
        ) : (
            <CartPriceButton onClick={props.onClickCart}>
              Beli
            </CartPriceButton>
          )}
      </CartRightTextBox>
    </CartBox>
  );
}

Card.defaultProps = {
  verified: false,
};

Card.propTypes = {
  title: PropTypes.oneOfType([
    PropTypes.string.isRequired,
    PropTypes.node.isRequired,
  ]).isRequired,
  verified: PropTypes.bool,
  price: PropTypes.number.isRequired,
  image: PropTypes.string.isRequired,
};

export default Card;

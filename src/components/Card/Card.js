import React from 'react';
import PropTypes from 'prop-types';
import {
  Box,
  ImageContainer,
  ImageComponent,
  SaleContainer,
  Sale,
  Image,
  Title,
  RightTextBox,
  DiscountPrice,
  SavePrice,
  Price,
  DiscountPriceContainer,
  PriceContainer,
  PriceButton,
  AmountContainer,
  NominalAmount
} from './styles';
import { currencyFormatter } from "../../utils/helper";

import { LazyLoadImage } from 'react-lazy-load-image-component';
import 'react-lazy-load-image-component/src/effects/blur.css';

function Card(props) {
  return (
    <Box onClick={props.onClickCard}>
      <ImageContainer>
        <ImageComponent>
          <LazyLoadImage
            alt={props.image.alt}
            height="160px"
            src={props.image} // use normal <img> attributes as props
            width="100%"
            effect="blur"
          />
          {/* <Image
            key={props.key}
            className="sampleImage"
            src={props.image}
            alt="sample image"
            height="160px"
            width="100%"
          /> */}
        </ImageComponent>
        <SaleContainer>
          {props.goods.discount ? <Sale>Promo</Sale> : null}
        </SaleContainer>
      </ImageContainer>
      <RightTextBox>
        <Title>{props.title}</Title>
        {/* <ContentText>{props.detail}</ContentText> */}
        {props.goods.discount ? (
          <DiscountPriceContainer>
            <DiscountPrice>
              {currencyFormatter.format(props.price)}/{props.minQuantity}{' '}
              {props.unit}
            </DiscountPrice>
            <SavePrice>
              Hemat{' '}
              {currencyFormatter.format(props.price - props.goods.discount)}
            </SavePrice>
            <Price>
              {currencyFormatter.format(props.goods.discount)}/
              {props.minQuantity} {props.unit}
            </Price>
          </DiscountPriceContainer>
        ) : (
          <PriceContainer>
            <Price>
              {currencyFormatter.format(props.price)}/{props.minQuantity}{' '}
              {props.unit}
            </Price>
          </PriceContainer>
        )}
      </RightTextBox>
      {props.cartList.find((row) => row.goodsId === props.goods.id) ? (
        <AmountContainer>
          <Image
            className="sampleImage"
            src={require('../../assets/images/minus.svg')}
            alt="sample image"
            height="38px"
            widht="38px"
            onClick={props.onClickReduceItems}
          />
          <NominalAmount>
            {props.cartList.find((row) => row.goodsId === props.goods.id).items}
          </NominalAmount>
          <Image
            className="sampleImage"
            src={require('../../assets/images/plus.svg')}
            alt="sample image"
            height="38px"
            widht="38px"
            onClick={props.onClickAddItems}
          />
        </AmountContainer>
      ) : (
        <PriceButton onClick={props.onClickCart}>Beli</PriceButton>
      )}
    </Box>
  );
}

Card.defaultProps = {
  verified: false,
};

Card.propTypes = {
  title: PropTypes.oneOfType([
    PropTypes.string.isRequired,
    PropTypes.node.isRequired,
  ]).isRequired,
  verified: PropTypes.bool,
  price: PropTypes.number.isRequired,
  image: PropTypes.string.isRequired,
};

export default Card;

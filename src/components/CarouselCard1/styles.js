import styled from 'styled-components';
import { Carousel } from 'react-bootstrap';

export const Box = styled.div`
  max-width: 395px;
  width: 100%;
  height: 252px;
  display: flex;
  flex-direction: row;
  justify-content: center;
  align-items: center;
  margin: auto;
  @media (max-width: 400px) {
    max-width: 340px;
  };
`;

export const ImageContainer = styled(Carousel.Item)`
  width: 100%;
  background-size: cover;
  background-position: 50% 50%;
`;

export const Image = styled.img`
  height: 210px;
  width: 375px;

  @media (max-width: 400px) {
    height: 190px;
    width: 340px
  };

  @media (max-width: 370px) {
    height: 175px;
    width: 320px
  };

  @media (max-width: 350px) {
    height: 165px;
    width: 295px
  };

  @media (max-width: 320px) {
    height: 151px;
    width: 270px
  };
`;
import React from 'react';
import PropTypes from 'prop-types';
import { Carousel } from 'react-bootstrap';
import {
  Box,
  ImageContainer,
  Image
} from './styles';
import { BANNER_MODEL } from "utils/constants";
import { getRequestURL } from "utils/request";

import { LazyLoadImage } from 'react-lazy-load-image-component';
import 'react-lazy-load-image-component/src/effects/blur.css';

function CarouselCard(props) {
  function getImage(url) {
    return `${getRequestURL(BANNER_MODEL)}/${url}/image?is_compressed=true`;
  }
  return (
    <Box>
      <Carousel>
        {
          props.listBanner && props.listBanner.map((obj, index) => {
            return (
              <ImageContainer key={index}>
                {/* <Img
                    className="sampleImage"
                    src={getImage(obj._id)}
                    alt="sample image"
                    height="210px"
                    widht="100%"
                  /> */}
                {/* <Image
                  className="sampleImage"
                  src={getImage(obj._id)}
                  alt="sample image"
                /> */}
                <LazyLoadImage
                  alt={getImage(obj._id).alt}
                  height="auto"
                  src={getImage(obj._id)} // use normal <img> attributes as props
                  width="100%"
                  effect="blur"
                />
              </ImageContainer>
            );})
        }
      </Carousel>
    </Box>
  );
}

CarouselCard.defaultProps = {
  verified: false,
};

CarouselCard.propTypes = {
  listBanner: PropTypes.oneOfType([PropTypes.bool, PropTypes.array]),
};

export default CarouselCard;

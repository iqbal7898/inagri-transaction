/*
 * HomePage Messages
 *
 * This contains all the text for the HomePage component.
 */
import { defineMessages } from 'react-intl';

export const scope = 'inaventory-marketplace.components.Header';

export default defineMessages({
  title: {
    id: `${scope}.title`,
    defaultMessage: 'Inaventory',
  },
  email: {
    id: `${scope}.email`,
    defaultMessage: 'Email: halo@inaventory.co.id',
  },
  home: {
    id: `${scope}.home`,
    defaultMessage: 'Home',
  },
  menu: {
    service: {
      id: `${scope}.service`,
      defaultMessage: 'Layanan Kami',
    },
    search_warehouse: {
      id: `${scope}.search_warehouse`,
      defaultMessage: 'Cari Gudang',
    },
    advertisement: {
      id: `${scope}.advertisement`,
      defaultMessage: 'Iklan Butuh Gudang',
    },
    partner: {
      id: `${scope}.partner`,
      defaultMessage: 'Mitra Gudang',
    },
    wms: {
      id: `${scope}.wms`,
      defaultMessage: 'WMS',
    },
    journal: {
      id: `${scope}.journal`,
      defaultMessage: 'Jurnal',
    },
  },
  login: {
    id: `${scope}.button.login`,
    defaultMessage: 'Masuk',
  },
  register: {
    id: `${scope}.button.register`,
    defaultMessage: 'Daftar',
  },
  type: {
    id: `${scope}.type`,
    defaultMessage: 'Warehouse Type',
  },
});

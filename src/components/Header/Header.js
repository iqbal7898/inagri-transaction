import React, { memo } from 'react';
// import { FormattedMessage } from 'react-intl';
// import { /* Button, */ Modal } from 'react-bootstrap';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { compose } from 'redux';
import Img from '../Img/Img';
import SearchIcon from '@material-ui/icons/Search';
import { createStructuredSelector } from "reselect";
import {
  searchGood,
  searchData
} from "containers/HomePage/actions";
import {
  makeSelectSearch
} from "containers/HomePage/selectors";

import * as Styles from './styles';
// import messages from './messages';

function Header({
  navigationList,
  onSearchData,
  onSearchGood,
  pathname,
  setPathname,
  back,
  title,
  homePath,
  history,
}) {
  return (
    <Styles.Static>
      {console.log('pathname', pathname)}
      {
        pathname === '/' ? (
          <Styles.Wrapper1>
            <Styles.UpSide>
              <Styles.LeftSide>
                <Link to="/" onClick={() => setPathname(homePath)}>
                  <Img
                    className="insanLogo"
                    src={require('../../assets/images/inagri logo.png')}
                    height="37.49px"
                    width="160.27px"
                    alt="insan logo"
                  />
                </Link>
              </Styles.LeftSide>
              <Styles.RightSide>
                {localStorage.getItem('token') ? (
                  <Link to="/profile" onClick={() => setPathname('Profile')}>
                    <Styles.img>
                      <Img
                        className="insanLogo"
                        src={require('../../assets/images/man-avatar.svg')}
                        height="25px"
                        width="25px"
                        alt="insan logo"
                      />
                    </Styles.img>
                  </Link>
                ) : (
                    <Link to="/login" onClick={() => setPathname('Login')}>
                      <Styles.img>
                        <Img
                          className="insanLogo"
                          src={require('../../assets/images/enter.svg')}
                          height="25px"
                          width="25px"
                          alt="insan logo"
                        />
                      </Styles.img>
                    </Link>
                )}

                <a href="https://api.whatsapp.com/send?phone=6283820644526">
                  <Img
                    className="whatsappLogo"
                    src={require('../../assets/images/whatsapp.svg')}
                    height="25px"
                    width="25px"
                    alt="whatsapp logo"
                  />
                </a>
              </Styles.RightSide>
            </Styles.UpSide>
            <Styles.DownSide>
              <Styles.SearchContainer>
                <Styles.SearchComponent>
                  <Styles.SearchContent
                    onClick={() => {
                      document.getElementById("searchinput").click();
                    }}
                    // onClick={() => history.push(`/search`)}
                  >
                    <SearchIcon style={{
                      marginRight: '7px',
                      color: 'rgb(127, 126, 126)',
                      fontSize: '23px',
                    }} />

                    <Styles.SearchInput
                      id="searchinput"
                      type="text"
                      onChange={event => {
                        // onSearchData(event);
                        onSearchGood(event);
                      }}
                      placeholder="Cari Produk Disini"
                    />
                    {/* <Styles.SearchInput>
                      Cari Produk Disini
                    </Styles.SearchInput> */}
                  </Styles.SearchContent>
                </Styles.SearchComponent>
              </Styles.SearchContainer>
            </Styles.DownSide>
          </Styles.Wrapper1>
        ) : (
            <Styles.Wrapper2>
              <Styles.MenuHeader>
                <Styles.NavbarLeft>
                  <Link to={back} onClick={() => setPathname(homePath)}>
                    <Img
                      className="backLogo"
                      src={require('../../assets/images/back.svg')}
                      height="30px"
                      width="30px"
                      alt="back logo"
                    />
                  </Link>
                </Styles.NavbarLeft>
                <Styles.NavbarTitle>
                  <Styles.MenuText>
                    {title}
                  </Styles.MenuText>
                </Styles.NavbarTitle>
              </Styles.MenuHeader>
            </Styles.Wrapper2>
          )}
    </Styles.Static>
  )
}

Header.propTypes = {
  navigationList: PropTypes.arrayOf(PropTypes.objectOf(PropTypes.string)),
  pathname: PropTypes.string,
  homePath: PropTypes.string,
  setPathname: PropTypes.func,
  history: PropTypes.object,
};

const mapStateToProps = createStructuredSelector({
  search: makeSelectSearch(),
});

function mapDispatchToProps(dispatch) {
  return {
    onSearchData: event => dispatch(searchData(event.target.value)),
    onSearchGood: event => dispatch(searchGood(event.target.value)),
  };
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

export default compose(
  withConnect,
  memo,
)(Header);

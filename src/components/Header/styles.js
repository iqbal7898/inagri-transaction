import styled from 'styled-components';

export const Static = styled.div`
  position: fixed;
  z-index: 99;
  width: 100%;
  max-width: 480px;
  justify-content: center;
  display: flex;
  box-shadow: rgba(57, 63, 72, 0.3) 0px 1px 3px;
`;

export const Wrapper1 = styled.div`
  max-width: 480px;
  width: 100%;
  background-color: #ffffff;
  padding-top: 25px;
  padding-bottom: 15px;
`;

export const Wrapper2 = styled.div`
  max-width: 470px;
  width: 100%;
  background-color: #ffffff;
  padding-bottom: 15px;
`;

export const UpSide = styled.div`
  z-index: 1;
  display: flex;
  line-height: 46px;
  flex-direction: row;
  text-align: center;
`;

export const LeftSide = styled.div`
  left: 0;
  position: absolute;
  display: flex;
  align-items: center;
  padding: 0 16px;
`;

export const RightSide = styled.div`
  right: 0;
  position: absolute;
  display: flex;
  align-items: center;
  padding: 0 16px;
`;

export const img = styled.div`
    margin-right: 20px;
`;

export const DownSide = styled.div`
  z-index: 1;
  display: flex;
  line-height: 46px;
  text-align: left;
  margin-top: 60px;
  width: 100%;
  height: 35px;
`;

export const SearchContainer = styled.div`
  width: 100%;
  height: 35px;
  justify-content: center;
  align-items: center;
  padding: 0 16px;
`;

export const SearchComponent = styled.div`
  height: 35px;
  background-color: rgb(234, 234, 234);
  border-radius: 5px;
  border-color: rgb(234, 234, 234);
  justify-content: flex-start;
  cursor: default;
  text-transform: capitalize;
`;

export const SearchContent = styled.div`
  align-items: center;
  color: inherit;
  display: flex;
  flex: 1 0 auto;
  justify-content: inherit;
  line-height: normal;
  position: relative;
  padding: 5px 16px;
`;

export const SearchText = styled.div`
  color: rgb(161, 161, 161);
  font-size: 13px;
  font-weight: normal;
  text-align: left;
  margin-top: 4px;
`;

export const SearchInput = styled.input`
  height: 30px;
  weight: 100%;
  background-color: rgb(234, 234, 234);
  border-color: transparent;
  justify-content: flex-start;
  cursor: default;
  text-transform: capitalize;
  box-shadow: 0px;
  font-family: 'Baloo 2'
`;

export const MenuHeader = styled.div`
  z-index: 1;
  display: flex;
  height: 60px;
  flex-direction: row;
  text-align: center;
`;

export const MenuText = styled.div`
  margin-top: 20px;
  font-family:'Baloo 2';
  font-size: 20px;
  font-weight: 400;
  font-stretch: normal;
  font-style: normal;
  line-height: 1.3;
  letter-spacing: normal;
  text-align: left;
  color: #212121;
`;

export const NavbarLeft = styled.div`
  top: 0;
  bottom: 0;
  left: 0;
  position: absolute;
  display: flex;
  align-items: center;
  padding: 0 20px;
`;

export const NavbarTitle = styled.div`
  max-width: 60%;
  margin: 0 auto;
  color: #323233;
  font-weight: 500;
  font-size: 16px;
`;
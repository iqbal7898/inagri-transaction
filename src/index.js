import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { ConnectedRouter } from 'connected-react-router';
import history from './utils/history';
import './index.css';
import './assets/theme/bootstrap.min.css';
import App from './containers/App/App.js';
import * as serviceWorker from './serviceWorker';
import configureStore from './configureStore';

// Create redux store with history
const initialState = {};
const store = configureStore(initialState, history);

ReactDOM.render(
  // <React.StrictMode>
    <Provider store={store}>
        <ConnectedRouter history={history}>
          <App />
        </ConnectedRouter>  
    </Provider>,
  // </React.StrictMode>,
  document.getElementById('root'),
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.register();

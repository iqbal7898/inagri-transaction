import axios from "axios";

/**
 * Defines request urls
 *
 * @param  {string} model   URL of the requested model
 *
 * @return {string}         An API url for the requested model
 */

// export const getRequestURL = (model) => `https://${model}`;
export const getRequestURL = (model) => `https://arcane-basin-66560.herokuapp.com/https://${model}`;

/**
 * Requests a URL, returning a promise
 *
 * @param  {string} url       The URL we want to request
 * @param  {object} [options] The options we want to pass to "fetch"
 *
 * @return {object}           The response data
 */
export function request(url, options) {
  return axios.request({
    url,
    timeout: 10000,
    ...options
  });
}

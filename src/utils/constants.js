export const RESTART_ON_REMOUNT = "@@saga-injector/restart-on-remount";
export const DAEMON = "@@saga-injector/daemon";
export const ONCE_TILL_UNMOUNT = "@@saga-injector/once-till-unmount";

// API Models
export const ROOT_URL = 'https://dashboard.inagri.asia';
export const ROOT_API = 'data.inagri.asia';

export const ACCOUNT_MODEL = `${ROOT_API}/account`;
export const TRANSACTION_MODEL = `${ROOT_API}/transaction/transactions`;
